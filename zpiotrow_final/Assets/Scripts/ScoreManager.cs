﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public Text scoreText;
    public Text resourcesText;

    public float scoreCount;
    public float resourcesCount;

    public float pointsPerSecond;

    public bool scoreIncreasing;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(scoreIncreasing)
        {
            scoreCount += pointsPerSecond * Time.deltaTime;
        }
        scoreText.text = "Score: " + Mathf.Round(scoreCount);
        resourcesText.text = "Resources: " + Mathf.Round(resourcesCount);   
    }

    public void AddResources (int resourcesToAdd)
    {
        resourcesCount += resourcesToAdd;
    }
}
