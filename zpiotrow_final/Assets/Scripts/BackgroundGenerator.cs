﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundGenerator : MonoBehaviour
{
    public GameObject theBackground;
    public Transform generationPoint;
    public float distanceBetween;

    private float backgroundWidth;
    // Start is called before the first frame update
    void Start()
    {
        backgroundWidth = 21f;//theBackground.transform.position.x;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x < generationPoint.position.x)
        {
            transform.position = new Vector3(transform.position.x + backgroundWidth, transform.position.y, transform.position.z);
            var prefab = Instantiate(theBackground, transform.position, transform.rotation);
            prefab.name = theBackground.name;
        }
    }
}
