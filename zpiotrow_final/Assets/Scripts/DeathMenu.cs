﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathMenu : MonoBehaviour
{
    public string mainMenuLevel;
    public void RestartGame()//player plays again.
    {
        FindObjectOfType<GameManager>().Reset();

    }

    public void QuitToMain() //player presses main menu
    {
        SceneManager.LoadScene(mainMenuLevel);
    }
    
    public void QuitPlaying() //player presses quit
    {
        Application.Quit();
        UnityEditor.EditorApplication.isPlaying = false;

    }
}
