﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinGenerator : MonoBehaviour
{
    public ObjectPooler resourcePool;

    public float distanceBetweenResources;

    public void SpawnResources(Vector3 startPosition)
    {
        GameObject resource1 = resourcePool.GetPooledObject();
        resource1.transform.position = startPosition;
        resource1.SetActive(true);

        GameObject resource2 = resourcePool.GetPooledObject();
        resource2.transform.position = new Vector3(startPosition.x + distanceBetweenResources, startPosition.y, startPosition.z);
        resource2.SetActive(true);

        GameObject resource3 = resourcePool.GetPooledObject();
        resource3.transform.position = new Vector3(startPosition.x + distanceBetweenResources + distanceBetweenResources, startPosition.y, startPosition.z);
        resource3.SetActive(true);
    }
}
