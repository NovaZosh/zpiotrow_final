﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundDestroyer : MonoBehaviour
{

    public GameObject backgroundDestructionPoint;

    // Start is called before the first frame update
    void Start()
    {
        backgroundDestructionPoint = GameObject.Find("BackgroundDestructionPoint");
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x < backgroundDestructionPoint.transform.position.x)
        {
            Destroy(gameObject);
        }
    }
}
