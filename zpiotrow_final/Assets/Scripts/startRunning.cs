﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class startRunning : MonoBehaviour
{
   public void startGame(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

}
