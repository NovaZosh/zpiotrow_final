﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public Transform platformGenerator;
    private Vector3 platformStartPoint;

    public Transform backgroundGenerator;
    private Vector3 backgroundStartPoint;


    public PlayerControl thePlayer;
    private Vector3 playerStartPoint;


    private PlatformDestroyer[] platformList;
    private ScoreManager theScoreManager;

    public DeathMenu theDeathScreen;

    // Start is called before the first frame update
    void Start()
    {
        platformStartPoint = platformGenerator.position;
        playerStartPoint = thePlayer.transform.position;

        backgroundStartPoint = backgroundGenerator.position;
        theScoreManager = FindObjectOfType<ScoreManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoseMenu()
    {
        theScoreManager.scoreIncreasing = false;
        thePlayer.gameObject.SetActive(false);

        theDeathScreen.gameObject.SetActive(true);
        //StartCoroutine("LoseMenuCo");
    }

    public void Reset()
    {
        theDeathScreen.gameObject.SetActive(false);
        platformList = FindObjectsOfType<PlatformDestroyer>();
        for (int i = 0; i < platformList.Length; i++)
        {
            platformList[i].gameObject.SetActive(false);
        }

        thePlayer.transform.position = playerStartPoint;
        platformGenerator.position = platformStartPoint;
        backgroundGenerator.position = backgroundStartPoint;
        thePlayer.gameObject.SetActive(true);
        theScoreManager.scoreCount = 0f;
        theScoreManager.scoreIncreasing = true;
        theScoreManager.resourcesCount = 0f;
    }
    /*public IEnumerator LoseMenuCo()
    {
        
        yield return new WaitForSeconds(0.6f);
        platformList = FindObjectsOfType<PlatformDestroyer>();
        for(int i = 0; i < platformList.Length; i++)
        {
            platformList[i].gameObject.SetActive(false);
        }
        
        thePlayer.transform.position = playerStartPoint;
        platformGenerator.position = platformStartPoint;
        backgroundGenerator.position = backgroundStartPoint;
        thePlayer.gameObject.SetActive(true);

    }*/
}
